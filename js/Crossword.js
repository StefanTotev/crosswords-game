class Crossword
{

    static directions = [
        {x: 0,  y: 1},  // vertical straight
        {x: 0,  y: -1}, // vertical reversed
        {x: 1,  y: 0},  // horizontal straight
        {x: 1,  y: 1},  // diagonal right down
        {x: 1,  y: -1}, // diagonal right up
        {x: -1, y: 0},  // horizontal reversed
        {x: -1, y: 1},  // diagonal left down
        {x: -1, y: -1}, // diagonal left up
    ];

    constructor(selector) {

        // Canvas
        this.canvas = document.querySelector(selector);
        this.ctx = this.canvas.getContext("2d");

        // Background and border colours
        this.strokeStyle = "#000000";
        this.fillStyle = "rgba(175, 179, 135, 0.33)";
        this.defaultFillStyle = "rgba(175, 179, 135, 0.33)";
        this.defaultHighlightFillStyle = "rgba(255, 255, 255, 0.4)";

        // Mapping variables
        this.tileMap = [];
        this.placedWordsCoords = [];
        this.words = [];

        // Statistics
        this.hitsCounter = 0;
        this.startTime = null;
        this.timeoutHandler = null;
        this.debugMode = false;
    }

    setBoardSize(width, height, tileSize) {
        // Board and tile sizes
        this.tileSize = tileSize;
        this.totalTilesX = width;
        this.totalTilesY = height;

        this.canvas.setAttribute('width', width * tileSize);
        this.canvas.setAttribute('height', height * tileSize);
    }

    start(debugMode) {
        this.debugMode = !!(debugMode);
        this.clearStats();

        this.showCanvas();
        this.initialise();
        this.initialiseEventListeners();
        this.draw();
    }

    initialise() {
        for ( let y = 0; y < this.totalTilesY; y++ ) {
            let cols = [];
            for (let x = 0; x < this.totalTilesX; x++) {
                cols[x] = {
                    char: String.fromCharCode(this.random(65, 91)),
                    highlight: false,
                    color: this.defaultFillStyle,
                    swapped: false
                };
            }
            this.tileMap[y] = cols;
        }

        this.placeWords();
    }

    initialiseEventListeners() {
        this.canvas.addEventListener('click', this.onClickHandler);
    }

    placeWords() {
        this.words.sort((a, b) => b.length - a.length);

        let index = 0;
        while ( index < this.words.length ) {
            const word = this.words[index].replace(/\s+/gi, '');
            const len = word.length;
            // const color = 'rgb(' + this.random(0, 255) + ',' + this.random(0, 255) + ',' + this.random(0, 255) + ')';
            const color = 'hsl(' + (index * 70) + ', ' + (50 + (this.random(10, 20) * this.random(-1, 1))) + '%, ' + (50 + (this.random(10, 20) * this.random(-1, 1))) + '%)';
            const coords = this.getRandomStartPosition();

            let pos = this.findProperDirection(coords, word);
            let y = pos.coords.y;
            let x = pos.coords.x;

            for ( let i = 0; i < len; i++ ) {
                if ( !this.tileMap[y][x].swapped ) {
                    this.placedWordsCoords.push({ x: x, y: y });
                }

                this.tileMap[y][x] = {
                    char: word[i].toUpperCase(),
                    highlight: this.debugMode,
                    color: color,
                    swapped: true
                };

                x += pos.directions.x;
                y += pos.directions.y;
            }
            index++;
        }
    }

    findProperDirection(coords, newWord, failedDirections = []) {
        const randomDirection = this.getRandomDirection();
        const directions = randomDirection.direction;
        failedDirections.push(randomDirection.index);

        let length = newWord.length;
        let dX = coords.x;
        let dY = coords.y;

        for ( let i = 0; i < length; i++ ) {
            if ( failedDirections.length < Crossword.directions.length ) {
                if ( (dX >= 0 && dX < this.totalTilesX) && (dY >= 0 && dY < this.totalTilesY) ) {
                    let currentTile = this.tileMap[dY][dX];

                    if ( currentTile.swapped ) {
                        if ( currentTile.char.toLowerCase() !== newWord[i].toLowerCase() ) {
                            console.log('Swapped letter - different letters. Word: ', newWord, ' Directions: ', directions);
                            return this.findProperDirection(coords, newWord, failedDirections);
                        }
                    }
                } else {
                    console.log('dX and dY out of bounds. Word: ', newWord, ' Directions: ', directions);
                    return this.findProperDirection(coords, newWord, failedDirections);
                }
            } else {
                coords = this.getRandomStartPosition();

                console.log('All directions unavailable. New coords! Word: ', newWord, ' Coords: ', coords);
                return this.findProperDirection(coords, newWord);
            }

            dX += directions.x;
            dY += directions.y;
        }

        return {
            coords: coords,
            directions: directions
        };
    }

    getRandomDirection() {
        const total = Crossword.directions.length;
        const rng = this.random(0, total);

        return {index: rng, direction: Crossword.directions[rng]};
    }

    getRandomStartPosition() {
        let startX = null;
        let startY = null;

        do {
            startX = this.random(0, this.totalTilesX);
            startY = this.random(0, this.totalTilesY);
        } while ( this.tileMap[startY][startX].swapped );

        return { x: startX, y: startY };
    }

    addWords(words) {
        this.words = words;
    }

    addTile(x, y) {
        this.ctx.beginPath();
        this.ctx.strokeStyle = this.strokeStyle;
        this.ctx.fillStyle = this.fillStyle;
        this.ctx.rect(x, y, this.tileSize, this.tileSize);
        this.ctx.fill();
        this.ctx.stroke();
    }

    addText(text, x, y) {
        this.ctx.fillStyle = "white";
        this.ctx.font = (this.tileSize / 2) + "px sans-serif";
        this.ctx.textBaseline = "middle";
        this.ctx.textAlign = "center";
        this.ctx.fillText(text, x + (this.tileSize / 2), y + (this.tileSize / 2) + 2);
    }

    onClickHandler = (e) => {
        const mouseX = e.layerX;
        const mouseY = e.layerY;
        const pos = {};
        let currentTile;

        pos.x = Math.floor(mouseX / this.tileSize);
        pos.y = Math.floor(mouseY / this.tileSize);
        console.log('X = ', pos.x + 1, ' Y = ', pos.y + 1);

        currentTile = this.tileMap[pos.y][pos.x];
        if ( !currentTile.swapped ) {
            currentTile.highlight = !currentTile.highlight;
            currentTile.color = (currentTile.highlight) ? this.defaultHighlightFillStyle : this.defaultFillStyle;
        } else {
            if ( !currentTile.highlight ) {
                currentTile.highlight = !currentTile.highlight;
                this.hitsCounter++;
            }
        }

        this.draw();
    };

    random = (min, max) => {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    draw() {
        this.clearBoard();

        for ( let i = 0; i < this.tileMap.length; i++ ) {
            for (let j = 0; j < this.tileMap[0].length; j++ ) {
                let x = j * this.tileSize;
                let y = i * this.tileSize;

                this.fillStyle = this.defaultFillStyle;
                if ( this.tileMap[i][j].highlight ) {
                    this.fillStyle = this.tileMap[i][j].color;
                }

                this.addTile(x, y);
                this.addText(this.tileMap[i][j].char, x, y);
            }
        }

        this.timeoutHandler = setTimeout(this.checkGameCompleted, 200);
    }

    reset() {
        this.words = [];
        this.clearBoard();
        this.hideCanvas();
    }

    toggle() {
        for ( let i = 0; i < this.placedWordsCoords.length; i++ ) {
            let x = this.placedWordsCoords[i].x;
            let y = this.placedWordsCoords[i].y;
            this.tileMap[y][x].highlight = !this.tileMap[y][x].highlight;
        }
        this.draw();
    }

    showCanvas() {
        this.canvas.style.visibility = 'visible';
    }

    hideCanvas() {
        this.canvas.style.visibility = 'hidden';
    }

    clearBoard() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    checkGameCompleted = () => {
        if ( this.placedWordsCoords.length > 0 && this.placedWordsCoords.length === this.hitsCounter ) {
            let endTime = performance.now();
            let totalTime = Number((endTime - this.startTime) / 1000).toFixed(0);

            if ( confirm(`Total time: ${totalTime} sec. \nDo you want to restart the game?`) ) {
                this.start();
            }
        }
    };

    clearStats() {
        this.placedWordsCoords = [];
        this.hitsCounter = 0;
        this.startTime = performance.now();
        clearTimeout(this.timeoutHandler);
    }
}
