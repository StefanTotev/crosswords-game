class Word
{
    constructor() {
        this.wordsList = [];
    }

    addWord(word) {
        this.wordsList.push(word);
    }

    getAllWords() {
        return this.wordsList;
    }

    clear() {
        this.wordsList = [];
    }
}
