const btnGenerate = document.querySelector('#generate');
const btnReset = document.querySelector('#reset');
const btnToggle = document.querySelector('#toggle');
const debugMode = document.querySelector('#debugMode');
const inputField = document.querySelector('#inputField');
const list = document.querySelector('#list');
const boardWidth = document.querySelector('#width');
const boardHeight = document.querySelector('#height');
const boardScale = document.querySelector('#scale');

const words = new Word();
const crosswords = new Crossword('#game');

inputField.addEventListener('keyup', (e) => {
    e.preventDefault();

    if ( e.which === 13 ) {
        if ( !inputField.value.length ) return;

        words.addWord(inputField.value.trim());
        updateList();
        clearInput();
    }
});

btnGenerate.addEventListener('click', () => {
    crosswords.addWords(words.getAllWords());
    crosswords.setBoardSize(boardWidth.value, boardHeight.value, boardScale.value);
    crosswords.start(debugMode.checked);
});

btnReset.addEventListener('click', () => {
    words.clear();
    crosswords.reset();
    updateList();
    clearInput();
});

btnToggle.addEventListener('click', () => crosswords.toggle());


function createListNode(text) {
    let node = document.createElement('li');
    let textNode = document.createTextNode(text);
    node.appendChild(textNode);

    return node;
}

function updateList() {
    list.innerHTML = '';
    words.getAllWords().map((e) => list.appendChild(createListNode(e)));
}

function clearInput() {
    inputField.value = null;
    inputField.focus();
}
